#!/bin/bash
while getopts "h" option;#getopts get argumet h
do
case $option in
h) #if user write h will see help text 
echo "This script is used to replace words in files in the current dictionary and subdirectosies 

Example: `basename $0` <original phrase> <new phrase>"
#basename print name with removed path
exit #quit script after printing usage
;;
esac # esac mean end of case
done

for file in `find . -type f`
#will find all files in current dictionary and subdirectosies 
do
if [ `basename "$0"` != `basename "$file"` ]
#skript will not change words in this script - it will except this file
# $0 means this script
then

sed -i "s/$1/$2/g" $file 
# -i means that it replace inside file, $1,$1 are arguemnts
fi #fi means end if
done

