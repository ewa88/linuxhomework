#!/bin/bash
while getopts "h" option;#getopts get argumet h
do
case $option in
h) #if user write -h will see help text 
echo "This script is used to change the directory letter size. 

Usage: `basename $0` <input case letter> <output case letter> <file name pattern optional>
Example: `basename $0` lower upper fileName.txt"

#basename print name with removed path
exit #quit script after printing usage
;;
esac # esac means end of case
done

pattern=* # means that size letter will change in all files names
if [ $# = 3 ] #check if third argument exist
then
pattern=$3 #change variable pattern to given argument
fi

for file in `find * -maxdepth 0 -name "$pattern" -type f` #all files in current directory which names match to pattern
do
if [ `basename "$0"` != `basename "$file"` ]
then
mv $file `echo $file | tr [:"$1":] [:"$2":]` #move file to the same file with changed name
# tr-translate text with changed size letter
fi
done
