#!/bin/bash
while getopts "h" option; #getopts get argumet h
do
case $option in
h) #if user write -h will see help text 
echo "This script is used to change file number.

Example: `basename $0` "

#basename print name with removed path
exit #quit script after printing usage
;;
esac # esac means end of case
done

i=1
for file in `find * -maxdepth 0 -type f` #all files in current directory 
do
if [ `basename "$0"` != `basename "$file"` ]
#basename means that skript will except this file
then
newFileName="${i}_$file" #new file name starts with number and _
mv $file `echo $newFileName`; #move file to the same file with new name
i=`expr $i + 1` #incrementing variable i
fi
done
